let thumbnail = document.querySelectorAll(".imgTitle");
let modal = document.getElementsByClassName("modal");
let close = document.getElementsByClassName("close");
let prev = document.getElementById("prev");
let next = document.getElementById("next");
let n;

// function to show slides
const showSlides = (n) => {
  let i;
  let slides = document.getElementsByClassName("mySlides");
  if (n > slides.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = slides.length;
  }
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slides[slideIndex - 1].style.display = "block";
};

let slideIndex = 1;

// function to show current slide
const currentSlide = (n) => {
  showSlides((slideIndex = n));
};

// open the modal if thumbnail is clicked
const thumbnailClicked = () => {
  thumbnail.forEach((ele) => {
    ele.onclick = () => {
      modal[0].style.display = "block";
      modal[0].style.width = "100%";
      if (ele.id === "index1") {
        currentSlide(1); //show slide 1 when thumbnail 1 is clicked
      } else if (ele.id === "index2") {
        currentSlide(2); //show slide 2 when thumbnail 2 is clicked
      } else {
        currentSlide(3); //show slide 3 when thumbnail 3 is clicked
      }
    };
  });
};
thumbnailClicked();

// next or previous control
const plusSlides = (n) => {
  showSlides((slideIndex += n));
};

// function when next button is clicked
const nextClicked = () => {
  next.onclick = () => {
    plusSlides(1);
  };
};
nextClicked();

// prev button is clicked
const prevClicked = () => {
  prev.onclick = () => {
    plusSlides(-1);
  };
};
prevClicked();

// close the modal
const closeModal = () => {
  close[0].onclick = () => {
    modal[0].style.display = "none";
  };
};
closeModal();
